from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter.ttk import Treeview
from tkinter.ttk import Style
import sqlite3


class Database:
    def __init__(self, db1):
        self.conn = sqlite3.connect(db1)
        self.cur = self.conn.cursor()
        # self.cur.execute(
        #   "CREATE TABLE IF NOT EXISTS jobs (id INTEGER PRIMARY KEY, [Customer Name] text, Address text, City text, State text)")
        # self.conn.commit()

    def fetch(self, customername=''):
        self.cur.execute(
            "SELECT * FROM bids WHERE [Customer Name] LIKE ?", ('%' + customername + '%',))
        #print("SELECT * FROM bids WHERE [Customer Name] LIKE ?", ('%' + customername + '%',))
        rows = self.cur.fetchall()
        return rows

    def fetch2(self, query):
        self.cur.execute(query)
        rows = self.cur.fetchall()
        return rows

    def insert(self, customername, address, city, state, zip1, phone1, phone2, biddate, job, startdate, description, price):
        sqlin = "INSERT INTO bids VALUES "
        sqlin += "(NULL, '" + customername + "', '"
        sqlin += address + "', '"
        sqlin += city + "', '"
        sqlin += state + "', '"
        sqlin += zip1 + "','"
        sqlin += phone1 + "', '"
        sqlin += phone2 + "', '"
        sqlin += biddate + "', '"
        sqlin += job + "','"
        sqlin += startdate + "', '"
        sqlin += description + "', "
        sqlin += str(price) + ")"
        print(sqlin)
        self.cur.execute(sqlin)
        self.conn.commit()

    def remove(self, idx):
        sqldel = "DELETE FROM bids WHERE bidLineId=" + str(idx)
        #print(sqlDel)
        self.cur.execute(sqldel)
        self.conn.commit()
        clear_text(self)
        populate_list()

    def update(self, id1, customername, address, city, state, zip1, phone1, phone2, biddate, job, startdate, description, price):
        sqlup = "UPDATE bids SET [Customer Name] = '" + customername + "', "
        sqlup += "Address = '" + address + "', "
        sqlup += "City = '" + city + "' , "
        sqlup += "State = '" + state + "' , "
        sqlup += "Zip = '" + zip1 + "' , "
        sqlup += "[Phone 1] = '" + phone1 + "' , "
        sqlup += "[Phone 2] = '" + phone2 + "' , "
        sqlup += "[Bid Date] = '" + biddate + "' , "
        sqlup += "Job = '" + job + "', "
        sqlup += "[Start Date] = '" + startdate + "' , "
        sqlup += "Description = '" + description + "' , "
        sqlup += "Price =" + price
        sqlup += " WHERE bidLineId = " + str(id1)
        print(sqlup)
        self.cur.execute(sqlup)
        self.conn.commit()
        clear_text(self)
        populate_list()

    def __del__(self):
        self.conn.close()


def leave_buckeye():
    sys.exit()


def populate_list(customername=''):
    for i in job_tree_view.get_children():
        job_tree_view.delete(i)
    for row in db.fetch(customername):
        job_tree_view.insert('', 'end', values=row)


def populate_list2(query='select * from bids'):
    for i in job_tree_view.get_children():
        job_tree_view.delete(i)
    for row in db.fetch2(query):
        job_tree_view.insert('', 'end', values=row)


def select_job(event):  # this gets the values from the treeview row clicked and poplates the feilds frame variables
    try:
        index = job_tree_view.selection()[0]
        selected_item = job_tree_view.item(index)['values']
        bidLineId_entry.delete(0, END)
        bidLineId_entry.insert(END, selected_item[0])
        customername_entry.delete(0, END)
        customername_entry.insert(END, selected_item[1])
        Address_entry.delete(0, END)
        Address_entry.insert(END, selected_item[2])
        City_entry.delete(0, END)
        City_entry.insert(END, selected_item[3])
        State_entry.delete(0, END)
        State_entry.insert(END, selected_item[4])
        Zip_entry.delete(0, END)
        Zip_entry.insert(END, selected_item[5])
        Phone1_entry.delete(0, END)
        Phone1_entry.insert(END, selected_item[6])
        Phone2_entry.delete(0, END)
        Phone2_entry.insert(END, selected_item[7])
        BidDate_entry.delete(0, END)
        BidDate_entry.insert(END, selected_item[8])
        Job_entry.delete(0, END)
        Job_entry.insert(END, selected_item[9])
        StartDate_entry.delete(0, END)
        StartDate_entry.insert(END, selected_item[10])
        Description_Text.delete(1.0, END)
        Description_Text.insert(END, selected_item[11])
        #tempVal = '${:,.0f}'.format(selected_item[12])
        # Price_entry.insert(END,tempVal)
        Price_entry.delete(0, END)
        Price_entry.insert(END, selected_item[12])
        print(selected_item)
    except IndexError:
        pass


def remove_job(self=None):
    db.remove(selected_item[0])
    clear_text(self)
    populate_list()


def update_job(self=None):
    if customername_text.get() == '':
        messagebox.showerror('Alert', 'No named customer to update!')
        return
    print(selected_item[0], customername_text.get(), Address_text.get(),
              City_text.get(), State_text.get(), Phone1_text.get(),
              Phone2_text.get(), BidDate_text.get(), Job_text.get(),
              StartDate_text.get(), Description_Text.get(1.0, END),
              Price_text.get())
    db.update(selected_item[0], customername_text.get(), Address_text.get(),
              City_text.get(), State_text.get(), Zip_text.get(), Phone1_text.get(),
              Phone2_text.get(), BidDate_text.get(), Job_text.get(),
              StartDate_text.get(), Description_Text.get(1.0, END), Price_text.get())
    clear_text(self)
    populate_list()


def add_job(self=None):
    if customername_text.get() == '' or Job_text.get() == '' or Price_text.get() == '' or Description_Text.get(1.0,  END) == '':
        messagebox.showerror('Required Fields', 'Missing Customer Name, Job Number, Description or Price')
        return
    db.insert(customername_text.get(), Address_text.get(), City_text.get(),
              State_text.get(), Zip_text.get(), Phone1_text.get(),
              Phone2_text.get(), BidDate_text.get(), Job_text.get(),
              StartDate_text.get(), Description_Text.get(1.0,END),
              Price_text.get())
    clear_text()
    populate_list()


def clear_text(self):
    bidLineId_entry.delete(0, END)
    Address_entry.delete(0, END)
    customername_entry.delete(0, END)
    City_entry.delete(0, END)
    Zip_entry.delete(0, END)
    Phone1_entry.delete(0, END)
    Phone2_entry.delete(0, END)
    BidDate_entry.delete(0, END)
    StartDate_entry.delete(0, END)
    Description_Text.delete(1.0, END)
    Price_entry.delete(0, END)


def search_customername():
    customername = customername_search.get()
    populate_list(customername)




def execute_query():
    query = query_search.get()
    populate_list2(query)


# Main Start****************************************************************************
global selected_item
app = Tk()
db = Database(r"C:\sqlite\db\BuckeyeWork.db")
app.maxsize(width=1400, height=750)
app.rowconfigure(0, weight=1)
app.title('job Manager')
app.geometry('1400x750')


#TITLE ****************************************************
frame_title = Frame(app)  #, bg='red')
frame_title.grid(row=0, column=0, sticky=N, pady=5, padx=10)
topLabel = Label(frame_title, text="BUCKEYE SERVICES", font = ('Courier 20 underline'))
topLabel.grid(row=0, column=0, sticky=N, pady=5, padx=10)
#Search ****************************************************
frame_search = Frame(app)  # bg='yellow')  #create frame for serch content
# frame_search['borderwidth'] = 2
# frame_search['relief'] = 'sunken'

frame_search.grid(row=1, column=0,  pady=5, padx=10)
#-----------------------Customer Name Search -----------------------------------------------
#  lbl_search = Label(frame_search, text='Search by Customer Name', font=('bold', 12), pady=5)
#  lbl_search.grid(row=1, column=0, sticky=W)
search_btn = Button(frame_search, text='Search by Customer Name', width=21, font=('bold', 12), command=search_customername)
search_btn.grid(row=1, column=0, padx=20, sticky=W)
customername_search = StringVar()
customername_search_entry = Entry(frame_search, font=('bold', 12), textvariable=customername_search)
customername_search_entry.grid(row=1, column=1, sticky=W)
#------------------------Custome Query Searched---------------------------------------------
lbl_search = Label(frame_search, text='    ', font=('bold', 12), padx=10, pady=5)
lbl_search.grid(row=1, column=2, sticky=E)
search_query_btn = Button(frame_search, text='Search by Query', width=13, font=('bold', 12), command=execute_query)
search_query_btn.grid(row=1, column=3, sticky=W)
lbl_search = Label(frame_search, text='', font=('bold', 12), padx=10, pady=5)
lbl_search.grid(row=1, column=4, sticky=E)
query_search = StringVar()
query_search.set("Select * from bids")
query_search_entry = Entry(frame_search, font=('bold', 12), textvariable=query_search, width=50)
query_search_entry.grid(row=1, column=5, sticky=E)


#***************************  FIELDS FRAME  ***********************
frame_fields = ttk.Frame(app)
#frame_fields.config(background='pink')
frame_fields.grid(row=2, column=0)
#frame_fields.place(anchor="EW")
frame_fields['borderwidth'] = 1
frame_fields['relief'] = 'solid'
frame_fields['padding'] = (5, 10, 5, 10)
#^^^^^^^^^^^^^^^ BidLineID
bidLineId_text = StringVar()
bidLineId_label = Label(frame_fields, text='ID', font=('bold', 12))
bidLineId_label.grid(row=1, column=0, sticky=E)
bidLineId_entry = Entry(frame_fields, font=('bold', 12), textvariable=bidLineId_text)  # , state=DISABLED
bidLineId_entry.grid(row=1, column=1, sticky=W)
# ^^^^^^^^^^^^^^^^^^ Customer Name
customername_text = StringVar()
customername_label = Label(frame_fields, text='Customer Name', font=('bold', 12))
customername_label.grid(row=1, column=1, sticky=E)
customername_entry = Entry(frame_fields, font=('bold', 12), textvariable=customername_text)
customername_entry.grid(row=1, column=2, sticky=W)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Address
Address_text = StringVar()
#print(Address_text)
Address_label = Label(frame_fields, text='Address',  font=('bold', 12))
Address_label.grid(row=1, column=3, sticky=E, pady=1)
Address_entry = Entry(frame_fields, font=('bold', 12),  width=30, textvariable=Address_text)
Address_entry.grid(row=1, column=4, sticky=W)
#6666666666666666666666666  City
City_text = StringVar()
City_label = Label(frame_fields, text='City', font=('bold', 12), padx=2, pady=1)
City_label.grid(row=1, column=4, sticky=E)
City_entry = Entry(frame_fields, font=('bold', 12), textvariable=City_text)
City_entry.grid(row=1, column=5, sticky=W)
# 6666666666666666666666666666 State
State_text = StringVar()
State_label = Label(frame_fields, text='State', font=('bold', 12), padx=4, pady=1)
State_label.grid(row=1, column=5, sticky=E)
State_entry = Entry(frame_fields, font=('bold', 12), width=4, textvariable=State_text)
State_entry.grid(row=1, column=6, sticky=W)
# 66666666666666666666666666 Zip Entry(app,
Zip_text = StringVar()
Zip_label = Label(frame_fields, text='Zip', font=('bold', 12), padx=4, pady=1)
Zip_label.grid(row=1, column=6, sticky=E)
Zip_entry = Entry(frame_fields, font=('bold', 12), width=12, textvariable=Zip_text)
Zip_entry.grid(row=1, column=7, sticky=W)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^Phone1
Phone1_text = StringVar()
Phone1_label = Label(frame_fields, text='Phone 1', font=('bold', 12), padx=2, pady=1)
Phone1_label.grid(row=2, column=0, )
Phone1_entry = Entry(frame_fields, font=('bold', 12), textvariable=Phone1_text)
Phone1_entry.grid(row=2, column=1, sticky=W)
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^ Phone2
Phone2_text = StringVar()
Phone2_label = Label(frame_fields, text='Phone 2', font=('bold', 12), padx=2, pady=1)
Phone2_label.grid(row=2, column=1, sticky=E)
Phone2_entry = Entry(frame_fields, font=('bold', 12), textvariable=Phone2_text)
Phone2_entry.grid(row=2, column=2, sticky=W)
# ^^^^^^^^^^^^^^^^^^^^^^^^ Bid Date
BidDate_text = StringVar()
BidDate_label = Label(frame_fields, text='Bid Date', font=('bold', 12), padx=2, pady=1)
BidDate_label.grid(row=2, column=3, sticky=E)
BidDate_entry = Entry(frame_fields, font=('bold', 12), textvariable=BidDate_text)
BidDate_entry.grid(row=2, column=4, sticky=W)
# ^^^^^^^^^^^^^^^^^^^^^^^^ Job
Job_text = StringVar()
Job_label = Label(frame_fields, text='Job #', font=('bold', 12), padx=2, pady=1)
Job_label.grid(row=2, column=4, sticky=E)
Job_entry = Entry(frame_fields, font=('bold', 12), width=6, textvariable=Job_text)
Job_entry.grid(row=2, column=5, sticky=W)
# ^^^^^^^^^^^^^^^^^^^ Start DAte
StartDate_text = StringVar()
StartDate_label = Label(frame_fields, text='Start Date', font=('bold', 12), padx=2)
StartDate_label.grid(row=2, column=5, sticky=E)
StartDate_entry = Entry(frame_fields, font=('bold', 12), textvariable=StartDate_text)
StartDate_entry.grid(row=2, column=6, sticky=W)
# ^^^^^^^^^^^^^^^^^^ Description
#Description_text = StringVar()
Description_label = Label(frame_fields, text='Description', font=('bold', 12), padx=2)
Description_label.grid(row=3, column=1, sticky=E)
Description_Text = Text(frame_fields, font=('bold', 12), height=4, width=90, wrap=WORD)
Description_Text.grid(row=3, column=2,  columnspan=8, sticky=SW)
x = Description_Text.get(1.0, END)
# ^^^^^^^^^^^^^^^^^^^^^^^^^ PRICE
Price_text = StringVar()
Price_label = Label(frame_fields, text='Price', font=('bold', 12), padx=2)
Price_label.grid(row=2, column=6, sticky=E)
Price_entry = Entry(frame_fields, font=('bold', 12), width=12, textvariable=Price_text)
Price_entry.grid(row=2, column=7, sticky=W)
# *************** Treeview Job Frame for primary data display**************************************************
frame_job = ttk.Frame(app)
frame_job.grid(row=4, column=0, pady=1, padx=10)
frame_job['padding'] = (5, 10)
frame_job['borderwidth'] = 1
frame_job['relief'] = 'solid'
columns = ['bidLineId', 'Customer Name', 'Address', 'City', 'State', 'Zip', 'Phone 1', 'Phone 2', 'Bid Date', 'Job',
           'Start Date', 'Description', 'Price']
style = Style()
style.configure("mystyle.Treeview", highlightthickness=0, bd=0, pady=20,  font=('Calibri', 11))  # Modify the font of the body
style.configure("mystyle.Treeview.Heading", font=('Calibri', 13, 'bold'))  # Modify the font of the headings
style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])  # Remove the borders
# Put treeview in Job Frame
job_tree_view = Treeview(frame_job, style="mystyle.Treeview", columns=columns,  show="headings", height=20)  # height is number of rows
# print(columns)
#job_tree_view.grid(font=('bold', 10))
job_tree_view.grid_rowconfigure(0, weight=1)
job_tree_view.grid_columnconfigure(0, minsize=30, weight=1)
# for col in columns[1:]:
job_tree_view.column('bidLineId', width=25)
job_tree_view.heading('bidLineId', text='ID')
job_tree_view.column('Customer Name', width=150)
job_tree_view.heading('Customer Name', text='CUSTOMER')
job_tree_view.column('Address', width=170)
job_tree_view.heading('Address', text='ADDRESS')
job_tree_view.column('City', width=50)
job_tree_view.heading('City', text='CITY')
job_tree_view.column('State', width=25)
job_tree_view.heading('State', text='ST')
job_tree_view.column('Zip', width=20)
job_tree_view.heading('Zip', text='ZIP')
job_tree_view.column('Phone 1', width=100)
job_tree_view.heading('Phone 1', text='Phone 1')
job_tree_view.column('Phone 2', width=100)
job_tree_view.heading('Phone 2', text='Phone 2')
job_tree_view.column('Bid Date', width=80)
job_tree_view.heading('Bid Date', text='Bid Date')
job_tree_view.heading('Job', text='Job')
job_tree_view.column('Job', width=40)
job_tree_view.column('Start Date', width=80)
job_tree_view.heading('Start Date', text='Start Date')
job_tree_view.column('Description', width=425)
job_tree_view.heading('Description', text='Description')
job_tree_view.column('Price', width=60)
job_tree_view.heading('Price', text='Price')
job_tree_view.bind('<<TreeviewSelect>>', select_job)
job_tree_view.pack(side="left", fill="y")
scrollbar = Scrollbar(frame_job, orient='vertical')
scrollbar.configure(command=job_tree_view.yview)
scrollbar.pack(side="right", fill="y")
job_tree_view.config(yscrollcommand=scrollbar.set)
# Buttoln Frames for database access ********************************************************
frame_btns = Frame(app)
frame_btns.grid(row=3, column=0)
frame_btns['borderwidth'] = 1
frame_btns['relief'] = 'solid'
# Add Job
add_btn = Button(frame_btns, text='Add Job', width=12, font=('Calibri', 11), command=add_job)
add_btn.grid(row=3, column=0, pady=5)
# 'Remove Job'
remove_btn = Button(frame_btns, text='Remove Job', width=12, font=('Calibri', 11), command=remove_job)
remove_btn.grid(row=3, column=1)
# 'Update Job
update_btn = Button(frame_btns, text='Update Job', width=12, font=('Calibri', 11), command=update_job)
update_btn.grid(row=3, column=2)
# 'Clear Input'
clear_btn = Button(frame_btns, text='Clear Input', width=12, font=('Calibri', 11), command=clear_text)
clear_btn.grid(row=3, column=4)
# 'EXIT'
quit_btn = Button(frame_btns, text='EXIT', width=12, font=('Calibri', 12, 'bold'), command=leave_buckeye)
quit_btn.grid(row=3, column=5)
# Populate data
populate_list()
# Start progCity
app.mainloop()
